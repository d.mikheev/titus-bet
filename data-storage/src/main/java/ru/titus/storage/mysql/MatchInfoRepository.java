package ru.titus.storage.mysql;

import org.springframework.data.repository.CrudRepository;
import ru.titus.model.sportradar.MatchInfo;


public interface MatchInfoRepository extends CrudRepository<MatchInfo, Long> {

}
