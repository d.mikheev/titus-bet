db.matchInfo.aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$unwind: { "path" : "$doc" }
		},

		// Stage 2
		{
			$unwind: { "path" : "$doc.data" }
		},

		// Stage 3
		{
			$match: { "doc.data._id" : 1.0 }
		},

		// Stage 4
		{
			$unwind: { "path" : "$doc.data.realcategories" }
		},

		// Stage 5
		{
			$unwind: { "path" : "$doc.data.realcategories.tournaments" }
		},

		// Stage 6
		{
			$unwind: { "path" : "$doc.data.realcategories.tournaments.matches" }
		},

		// Stage 7
		{
			$replaceRoot: { "newRoot" : "$doc.data.realcategories.tournaments.matches" }
		},

		// Stage 8
		{
			$match: { "$expr" : { "$eq" : [ "$_dt.date", { "$concat" : [ { "$dateToString" : { "format" : "%d/%m/", "date" : ISODate("2018-02-04T00:00:00.000+0000") } }, { "$substr" : [ { "$year" : ISODate("2018-02-07T00:00:00.000+0000") }, 2.0, 2.0 ] } ] } ] } }
		},
	],

	// Options
	{
		cursor: {
			batchSize: 50
		}
	}

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
