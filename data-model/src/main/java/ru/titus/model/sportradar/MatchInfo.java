package ru.titus.model.sportradar;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "match_info", schema = "matches_stats", catalog = "")
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MatchInfo {
    private long id;
    private Timestamp requestTime;
    private Integer matchid;
    private Byte localDerby;
    private String sex;
    private String home;
    private Integer homeTeamid;
    private String homeMediumName;
    private String away;
    private Integer awayTeamid;
    private String awayMediumName;
    private int mergeScore;
    private Integer weather;
    private Integer pitchCondition;
    private Integer temperature;
    private Integer wind;
    private Integer windAdvantage;
    private Integer stadiumid;
    private String stadiumName;
    private String stadiumUrl;
    private String stadiumCity;
    private String stadiumCountry;
    private Integer stadiumCapacity;
    private String tournamentSeasonTypeName;
    private String tournamentName;
    private Integer tournamentid;
    private String stadiumGoogleCoords;
    private Integer stadiumConstYear;

    public MatchInfo (){
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "request_time")
    public Timestamp getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Timestamp requestTime) {
        this.requestTime = requestTime;
    }

    @Basic
    @Column(name = "matchid")
    public Integer getMatchid() {
        return matchid;
    }

    public void setMatchid(Integer matchid) {
        this.matchid = matchid;
    }

    @Basic
    @Column(name = "localDerby")
    public Byte getLocalDerby() {
        return localDerby;
    }

    public void setLocalDerby(Byte localDerby) {
        this.localDerby = localDerby;
    }

    @Basic
    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "home")
    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    @Basic
    @Column(name = "homeTeamid")
    public Integer getHomeTeamid() {
        return homeTeamid;
    }

    public void setHomeTeamid(Integer homeTeamid) {
        this.homeTeamid = homeTeamid;
    }

    @Basic
    @Column(name = "homeMediumName")
    public String getHomeMediumName() {
        return homeMediumName;
    }

    public void setHomeMediumName(String homeMediumName) {
        this.homeMediumName = homeMediumName;
    }

    @Basic
    @Column(name = "away")
    public String getAway() {
        return away;
    }

    public void setAway(String away) {
        this.away = away;
    }

    @Basic
    @Column(name = "awayTeamid")
    public Integer getAwayTeamid() {
        return awayTeamid;
    }

    public void setAwayTeamid(Integer awayTeamid) {
        this.awayTeamid = awayTeamid;
    }

    @Basic
    @Column(name = "awayMediumName")
    public String getAwayMediumName() {
        return awayMediumName;
    }

    public void setAwayMediumName(String awayMediumName) {
        this.awayMediumName = awayMediumName;
    }

    @Basic
    @Column(name = "mergeScore")
    public int getMergeScore() {
        return mergeScore;
    }

    public void setMergeScore(int mergeScore) {
        this.mergeScore = mergeScore;
    }

    @Basic
    @Column(name = "weather")
    public Integer getWeather() {
        return weather;
    }

    public void setWeather(Integer weather) {
        this.weather = weather;
    }

    @Basic
    @Column(name = "pitchCondition")
    public Integer getPitchCondition() {
        return pitchCondition;
    }

    public void setPitchCondition(Integer pitchCondition) {
        this.pitchCondition = pitchCondition;
    }

    @Basic
    @Column(name = "temperature")
    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "wind")
    public Integer getWind() {
        return wind;
    }

    public void setWind(Integer wind) {
        this.wind = wind;
    }

    @Basic
    @Column(name = "windAdvantage")
    public Integer getWindAdvantage() {
        return windAdvantage;
    }

    public void setWindAdvantage(Integer windAdvantage) {
        this.windAdvantage = windAdvantage;
    }

    @Basic
    @Column(name = "stadiumid")
    public Integer getStadiumid() {
        return stadiumid;
    }

    public void setStadiumid(Integer stadiumid) {
        this.stadiumid = stadiumid;
    }

    @Basic
    @Column(name = "stadiumName")
    public String getStadiumName() {
        return stadiumName;
    }

    public void setStadiumName(String stadiumName) {
        this.stadiumName = stadiumName;
    }

    @Basic
    @Column(name = "stadiumUrl")
    public String getStadiumUrl() {
        return stadiumUrl;
    }

    public void setStadiumUrl(String stadiumUrl) {
        this.stadiumUrl = stadiumUrl;
    }

    @Basic
    @Column(name = "stadiumCity")
    public String getStadiumCity() {
        return stadiumCity;
    }

    public void setStadiumCity(String stadiumCity) {
        this.stadiumCity = stadiumCity;
    }

    @Basic
    @Column(name = "stadiumCountry")
    public String getStadiumCountry() {
        return stadiumCountry;
    }

    public void setStadiumCountry(String stadiumCountry) {
        this.stadiumCountry = stadiumCountry;
    }

    @Basic
    @Column(name = "stadiumCapacity")
    public Integer getStadiumCapacity() {
        return stadiumCapacity;
    }

    public void setStadiumCapacity(Integer stadiumCapacity) {
        this.stadiumCapacity = stadiumCapacity;
    }

    @Basic
    @Column(name = "tournamentSeasonTypeName")
    public String getTournamentSeasonTypeName() {
        return tournamentSeasonTypeName;
    }

    public void setTournamentSeasonTypeName(String tournamentSeasonTypeName) {
        this.tournamentSeasonTypeName = tournamentSeasonTypeName;
    }

    @Basic
    @Column(name = "tournamentName")
    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    @Basic
    @Column(name = "tournamentid")
    public Integer getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(Integer tournamentid) {
        this.tournamentid = tournamentid;
    }

    @Basic
    @Column(name = "stadiumGoogleCoords")
    public String getStadiumGoogleCoords() {
        return stadiumGoogleCoords;
    }

    public void setStadiumGoogleCoords(String stadiumGoogleCoords) {
        this.stadiumGoogleCoords = stadiumGoogleCoords;
    }

    @Basic
    @Column(name = "stadiumConstYear")
    public Integer getStadiumConstYear() {
        return stadiumConstYear;
    }

    public void setStadiumConstYear(Integer stadiumConstYear) {
        this.stadiumConstYear = stadiumConstYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchInfo that = (MatchInfo) o;
        return id == that.id &&
                Objects.equals(requestTime, that.requestTime) &&
                Objects.equals(matchid, that.matchid) &&
                Objects.equals(localDerby, that.localDerby) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(home, that.home) &&
                Objects.equals(homeTeamid, that.homeTeamid) &&
                Objects.equals(homeMediumName, that.homeMediumName) &&
                Objects.equals(away, that.away) &&
                Objects.equals(awayTeamid, that.awayTeamid) &&
                Objects.equals(awayMediumName, that.awayMediumName) &&
                Objects.equals(weather, that.weather) &&
                Objects.equals(pitchCondition, that.pitchCondition) &&
                Objects.equals(temperature, that.temperature) &&
                Objects.equals(wind, that.wind) &&
                Objects.equals(windAdvantage, that.windAdvantage) &&
                Objects.equals(stadiumid, that.stadiumid) &&
                Objects.equals(stadiumName, that.stadiumName) &&
                Objects.equals(stadiumUrl, that.stadiumUrl) &&
                Objects.equals(stadiumCity, that.stadiumCity) &&
                Objects.equals(stadiumCountry, that.stadiumCountry) &&
                Objects.equals(stadiumCapacity, that.stadiumCapacity) &&
                Objects.equals(tournamentSeasonTypeName, that.tournamentSeasonTypeName) &&
                Objects.equals(tournamentName, that.tournamentName) &&
                Objects.equals(tournamentid, that.tournamentid) &&
                Objects.equals(stadiumGoogleCoords, that.stadiumGoogleCoords) &&
                Objects.equals(stadiumConstYear, that.stadiumConstYear);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, requestTime, matchid, localDerby, sex, home, homeTeamid, homeMediumName, away, awayTeamid, awayMediumName, weather, pitchCondition, temperature, wind, windAdvantage, stadiumid, stadiumName, stadiumUrl, stadiumCity, stadiumCountry, stadiumCapacity, tournamentSeasonTypeName, tournamentName, tournamentid, stadiumGoogleCoords, stadiumConstYear);
    }
}
