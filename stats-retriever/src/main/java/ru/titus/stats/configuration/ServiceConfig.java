package ru.titus.stats.configuration;


import jayeson.lib.recordfetcher.DeltaCrawlerSession;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.titus.stats.service.merger.MatchMerger;
import ru.titus.stats.service.scorer.MatchScorer;
import ru.titus.stats.service.scorer.MatchScorerImpl;
import ru.titus.stats.service.merger.ScoringMatchMerger;
import ru.titus.stats.service.rest.SportRadarClient;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.util.Properties;


@Configuration
public class ServiceConfig {


    public final String statsBaseUrl = "https://wlc.fn.sportradar.com/";

    @Bean
    public SportRadarClient createSportRadarClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(statsBaseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(getUnsafeOkHttpClient())
                .build();
        return retrofit.create(SportRadarClient.class);
    }

    //FIXME: this is just a temporary workaround to make retrofit work with sportradar. Need to solve all the ssl problems
    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public DeltaCrawlerSession createDeltaCrawlerSession() {
        Properties systemProps = System.getProperties();
        //FIXME: JVM properties shouldn't be set here.
        //setup key stores for secure connections
        systemProps.put("javax.net.ssl.trustStore", "conf/client.ts");
        systemProps.put("javax.net.ssl.keyStore", "conf/client.ks");
        systemProps.put("javax.net.ssl.trustStorePassword", "password");
        systemProps.put("javax.net.ssl.keyStorePassword", "password");
        //setup the configuration file
        systemProps.put("deltaCrawlerSessionConfigurationFile", "conf/deltaCrawlerSession.json");
        return new DeltaCrawlerSession();
    }

    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        //Using gmail
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername("titusbet9080");
        mailSender.setPassword("orbit666");

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.starttls.enable", "true");
        javaMailProperties.put("mail.smtp.auth", "true");
        javaMailProperties.put("mail.transport.protocol", "smtp");
        javaMailProperties.put("mail.debug", "true");//Prints out everything on screen
        javaMailProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }

    @Bean
    public MatchMerger scoringMerger() {
        return new ScoringMatchMerger();
    }

    @Bean
    public MatchScorer matchScorer() {
        return new MatchScorerImpl();
    }

}
