package ru.titus.stats.service.rest;

import java.util.Date;

public interface InfoService {

    public String getMatchInfo(Date date);

    public String retrieveStats(long matchId);

    public String retrieveInfo(long matchId);

    public String getMatchSituation(long matchId);
}