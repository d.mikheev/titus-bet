package ru.titus.stats.service;

import jayeson.lib.recordfetcher.DeltaCrawlerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.feed.BookSubscriber;
import ru.titus.model.MergeControl;
import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.datafeed.MatchOdd;
import ru.titus.model.sportradar.Event;
import ru.titus.model.sportradar.MatchDetails;
import ru.titus.model.sportradar.MatchInfo;
import ru.titus.stats.service.email.EmailService;
import ru.titus.stats.service.merger.MatchMergingService;
import ru.titus.stats.service.rest.InfoService;
import ru.titus.storage.mysql.MatchDetailsRepository;
import ru.titus.storage.mysql.MatchInfoRepository;
import ru.titus.storage.mysql.MatchOddsRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class StatsCrawler {

    private static final Logger log = LoggerFactory.getLogger(StatsCrawler.class);

    @Autowired
    private final InfoService infoService;
    @Autowired
    private JsonParser jsonParser;
    @Autowired
    private EmailService emailService;
    @Autowired
    private List<MatchInfo> matchesInfo;
    @Autowired
    private MatchInfoRepository matchInfoRepository;
    @Autowired
    private MatchDetailsRepository matchDetailsRepository;
    @Autowired
    private MatchOddsRepository matchOddsRepository;
    @Autowired
    private DeltaCrawlerSession cs;
    @Autowired
    private BookSubscriber bookSubscriber;
    @Autowired
    private MatchMergingService matchMergingService;

    public StatsCrawler(InfoService infoService, DeltaCrawlerSession deltaCrawlerSession) {
        this.infoService = infoService;
        this.cs = deltaCrawlerSession;
        this.cs.connect();
    }

    public List<Event> getSportRadarLiveEvents() {
        List<Event> newLiveEvents = new ArrayList<>();
        String eventFullFeed = infoService.getMatchInfo(new Date());
        newLiveEvents = JsonParser.parseEvents(eventFullFeed);
        for (int i = 0; i < newLiveEvents.size(); i++) {
            String matchSituation = infoService.getMatchSituation(newLiveEvents.get(i).getMatchid());
            newLiveEvents.get(i).setMainTime(JsonParser.getMatchMainTime(matchSituation));
            newLiveEvents.get(i).setAdditionalTime(JsonParser.getMatchAddTime(matchSituation));
        }
        return newLiveEvents;
    }

    public void emailMergeControl() {
        List<Event> liveSportRadarMatches = getSportRadarLiveEvents();
        List<BookMatch> bookMatches = bookSubscriber.getBookMatches();
        List<MergeControl> mergeMatches = matchMergingService.getMergeControlList(bookMatches, liveSportRadarMatches);
        emailService.sendMail(mergeMatches);
    }

    public void retrieveAndSaveMatchData() {
        //get live events
        List<Event> liveSportRadarMatches = getSportRadarLiveEvents();
        //get quotes
        List<BookMatch> bookMatches = bookSubscriber.getBookMatches();
        //match quotes to live events
        List<BookMatch> mergeBookMatches = matchMergingService.deleteDifferences(bookMatches, liveSportRadarMatches);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        //process live matches with quotes
        for (BookMatch mergeBookMatch : mergeBookMatches) {
            processMatch(timestamp, mergeBookMatch);
        }
    }

    private void processMatch(Timestamp timestamp, BookMatch mergeBookMatch) {
        MatchDetails matchDetails = fetchMatchDetails(timestamp, mergeBookMatch);
        matchDetails = matchDetailsRepository.save(matchDetails);

        //create quotes
        for (MatchOdd bookMatchOdd : mergeBookMatch.getMatchOdds()) {
            MatchOdd matchOdd = extractMatchOdd(timestamp, mergeBookMatch, matchDetails, bookMatchOdd);
            matchOddsRepository.save(matchOdd);
        }

        //Create match general information if not exists
        if (matchInfoRepository.exists(matchDetails.getMatchid())) {
            MatchInfo matchInfo = fetchMatchInfo(timestamp, mergeBookMatch, matchDetails);
            matchInfoRepository.save(matchInfo);
        }
    }

    private MatchInfo fetchMatchInfo(Timestamp timestamp, BookMatch mergeBookMatch, MatchDetails matchDetails) {
        MatchInfo matchInfo = new MatchInfo();
        matchInfo = JsonParser.parseMatchInfo(infoService.retrieveInfo(matchDetails.getMatchid()));
        matchInfo.setRequestTime(timestamp);
        matchInfo.setMergeScore(mergeBookMatch.getMergeScore());
        return matchInfo;
    }

    private MatchOdd extractMatchOdd(Timestamp timestamp, BookMatch mergeBookMatch, MatchDetails matchDetails, MatchOdd bookMatchOdd) {
        MatchOdd matchOdd = new MatchOdd();
        matchOdd.setRequestTime(timestamp);
        matchOdd.setMatchid(mergeBookMatch.getMatchid());
        matchOdd.setDuration(mergeBookMatch.getDuration());
        matchOdd.setMatchDetailid(matchDetails.getId());
        matchOdd.setPivotValue(bookMatchOdd.getPivotValue());
        matchOdd.setPivotType(bookMatchOdd.getPivotType());
        matchOdd.setPivotBias(bookMatchOdd.getPivotBias());
        matchOdd.setLbType(bookMatchOdd.getLbType());
        matchOdd.setTimeType(bookMatchOdd.getTimeType());
        matchOdd.setRateEqual(bookMatchOdd.getRateEqual());
        matchOdd.setRateEqualUid(bookMatchOdd.getRateEqualUid());
        matchOdd.setRateOver(bookMatchOdd.getRateOver());
        matchOdd.setRateOverUid(bookMatchOdd.getRateOverUid());
        matchOdd.setRateUnder(bookMatchOdd.getRateUnder());
        matchOdd.setRateUnderUid(bookMatchOdd.getRateUnderUid());
        matchOdd.setSource(bookMatchOdd.getSource());
        matchOdd.setOddId(bookMatchOdd.getOddId());
        matchOdd.setOddType(bookMatchOdd.getOddType());
        matchOdd.setPivotString(bookMatchOdd.getPivotString());
        return matchOdd;
    }

    private MatchDetails fetchMatchDetails(Timestamp timestamp, BookMatch mergeBookMatch) {
        MatchDetails matchDetails = new MatchDetails();
        matchDetails = jsonParser.parseMatchDetails(infoService.retrieveStats(mergeBookMatch.getMatchid()));
        matchDetails.setDuration(mergeBookMatch.getDuration());
        matchDetails.setBookHost(mergeBookMatch.getHost());
        matchDetails.setBookGuest(mergeBookMatch.getGuest());
        matchDetails.setTimeMain(mergeBookMatch.getMainTime());
        matchDetails.setTimeAdd(mergeBookMatch.getAdditionalTime());
        matchDetails.setScoreHome(mergeBookMatch.getScoreHome());
        matchDetails.setScoreAway(mergeBookMatch.getScoreAway());
        matchDetails.setRequestTime(timestamp);
        return matchDetails;
    }

}
