package ru.titus.stats.service.email;

import ru.titus.model.MergeControl;

import java.util.List;


public interface EmailService {

    public void sendMail(List<MergeControl> mergeControlList);

    }