package ru.titus.stats.service.rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class SportRadarInfoService implements InfoService {
    private static final Logger logger = LoggerFactory.getLogger(SportRadarInfoService.class);
    //this service_id used in betradar widget
    private static final int MATCH_INFO_SERVICE_ID = 24;
    private final SportRadarClient client;

    @Autowired
    public SportRadarInfoService(SportRadarClient client) {
        this.client = client;
    }

    @Override
    public String retrieveStats(long matchId) {
        try {
            return client.detailsExtended(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String retrieveInfo(long matchId) {
        try {
            return client.matchInfo(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getMatchInfo(Date date) {
        try {
            //FIXME: probably the timezone issues can occur. Timezone needs to be specified explicitly
            long daysBetween = DAYS.between(LocalDate.now(), date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            String matchInfoJson = client.getMatchList(daysBetween, MATCH_INFO_SERVICE_ID).execute().body();
            return matchInfoJson;
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getMatchSituation(long matchId) {
        try {
            return client.getMatchSituation(matchId).execute().body();
        } catch (IOException e) {
            logger.error(e.toString(), e);
            throw new RuntimeException(e);
        }
    }
}
