package ru.titus.stats.service.scorer;

import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.sportradar.Event;

public class MatchScorerImpl implements MatchScorer {

    public static final int TOTAL_SCORE = 8;
    public static final int START_TIME_BONUS = 1;
    public static final int LEAGUE_BONUS = 1;
    public static final int LEAGUE_COUNTRY_FINE = -1;
    public static final int LEAGUE_COUNTRY_BONUS = 1;
    public static final int SCORE_BONUS = 1;
    public static final int DURATION_BONUS = 1;
    public static final int NAME_PART_BONUS = 1;
    public static final int NAME_BONUS = 6;
    public static final int NAME_FINE = -1;
    public static final int NAMES_FINE = -5;
    public static final int WATCHING_SCORE = 6;


    @Override
    public int score(BookMatch bookMatch, Event event) {
        Boolean result = false;
        int matchingScore = 0;
        int distanceScore = 0;
        int totalScore = 0;
        if (bookMatch.getLeague().contains(event.getLeagueCountry()))
            matchingScore = matchingScore + LEAGUE_COUNTRY_BONUS;
        else
            matchingScore = matchingScore + LEAGUE_COUNTRY_FINE;
        if (bookMatch.getLeague() == (event.getLeagueCountry() + " " + event.getLeagueName()))
            matchingScore = matchingScore + LEAGUE_BONUS;
        if (bookMatch.getStartTime() == event.getUts())
            matchingScore = matchingScore + START_TIME_BONUS;
        if ((bookMatch.getScoreHome() == event.getScoreHome()) && (bookMatch.getScoreAway() == event.getScoreAway()))
            matchingScore = matchingScore + SCORE_BONUS;
        if (bookMatch.getDuration() == event.getMainTime())
            matchingScore = matchingScore + DURATION_BONUS;
        distanceScore = stringDistanceScore(event.getHome(), bookMatch.getHost());
        distanceScore = distanceScore + stringDistanceScore(event.getAway(), bookMatch.getGuest());
        if (distanceScore < 0)
            totalScore = matchingScore + NAMES_FINE;
        else
            totalScore = matchingScore + distanceScore;
        return totalScore;
    }

    private int stringDistanceScore(String one, String two) {
        int distanceScore = 0;
        one = one.toUpperCase();
        two = two.toUpperCase();
        String oneMas[] = one.split(" ");
        if ((two.contains(one)) || (one.contains(two)))
            distanceScore = MatchScorerImpl.NAME_BONUS;
        else {
            for (String oneWord : oneMas) {
                if ((two.contains(oneWord)) && (oneWord != "FC"))
                    distanceScore = distanceScore + MatchScorerImpl.NAME_PART_BONUS;
            }
            if (distanceScore == 0)
                distanceScore = MatchScorerImpl.NAME_FINE;
        }
        return distanceScore;
    }
}
