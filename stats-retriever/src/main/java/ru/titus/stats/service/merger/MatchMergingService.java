package ru.titus.stats.service.merger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.titus.model.MergeControl;
import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.sportradar.Event;
import ru.titus.stats.service.scorer.MatchScorer;
import ru.titus.stats.service.scorer.MatchScorerImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MatchMergingService {


    @Autowired
    private MatchMerger merger;
    @Autowired
    private MatchScorer scorer;


    public List<BookMatch> deleteDifferences(List<BookMatch> allBookMatches, List<Event> liveSportRadarMatches) {
        List<BookMatch> crossingBookMatches = new ArrayList<BookMatch>();
        for (BookMatch bookMatch : allBookMatches) {
            Optional<BookMatch> matchOptional = merger.mergeMatch(bookMatch, liveSportRadarMatches);
            if (matchOptional.isPresent()) {
                crossingBookMatches.add(matchOptional.get());
            }

        }
        return crossingBookMatches;
    }

    public List<MergeControl> getMergeControlList(List<BookMatch> allBookMatches, List<Event> liveSportRadarMatches) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        List<MergeControl> mergeControlList = new ArrayList<MergeControl>();
        for (BookMatch bookMatch : allBookMatches) {
            for (Event liveSportRadarMatch : liveSportRadarMatches) {
                int score = scorer.score(bookMatch, liveSportRadarMatch);
                //FIXME: should not use the constants from Implementation
                if (score >= MatchScorerImpl.WATCHING_SCORE) {
                    MergeControl mergeControl = new MergeControl();
                    mergeControl.setRequestDate(timestamp);
                    mergeControl.setMergeScore(score);

                    mergeControl.setMatchid(liveSportRadarMatch.getMatchid());
                    mergeControl.setRadarHome(liveSportRadarMatch.getHome());
                    mergeControl.setRadarHomeScore(liveSportRadarMatch.getScoreHome());
                    mergeControl.setRadarAway(liveSportRadarMatch.getAway());
                    mergeControl.setRadarAwayScore(liveSportRadarMatch.getScoreAway());
                    mergeControl.setRadarLeagueCountry(liveSportRadarMatch.getLeagueCountry());
                    mergeControl.setRadarLeagueName(liveSportRadarMatch.getLeagueName());
                    mergeControl.setRadarMainTime(liveSportRadarMatch.getMainTime());
                    mergeControl.setRadarStartTime(liveSportRadarMatch.getUts());

                    mergeControl.setBookHome(bookMatch.getHost());
                    mergeControl.setBookHomeScore(bookMatch.getScoreHome());
                    mergeControl.setBookAway(bookMatch.getGuest());
                    mergeControl.setBookAwayScore(bookMatch.getScoreAway());
                    mergeControl.setBookLeague(bookMatch.getGuest());
                    mergeControl.setBookDuration(bookMatch.getDuration());
                    mergeControl.setBookStartTime(bookMatch.getStartTime());

                    mergeControlList.add(mergeControl);
                }
            }
        }

        return mergeControlList;
    }


}
