package ru.titus.stats.service.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import ru.titus.model.MergeControl;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.Timestamp;
import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    JavaMailSender mailSender;

    @Override
    public void sendMail(List<MergeControl> mergeControlList) {
        MimeMessagePreparator preparator = getMatchControlPreparator(mergeControlList);

        try {
            mailSender.send(preparator);
            System.out.println("Message Send...Hurrey");
        } catch (MailException ex) {
            System.err.println(ex.getMessage());
        }

    }

    private String getMessageText(List<MergeControl> mergeControlList){
        String result = "";
        String resultRadar = "";
        String resultBook = "";
        for (MergeControl mergeControl:mergeControlList) {
            result=result + mergeControl.getMatchid() +", mergeScore = "+ mergeControl.getMergeScore()+"\n";

            resultRadar = mergeControl.getRadarStartTime() + " " + mergeControl.getRadarMainTime() + " "
                    + mergeControl.getRadarHome() + " __ "+ mergeControl.getRadarAway() + " " +
                    + mergeControl.getRadarHomeScore() + " " + mergeControl.getRadarAwayScore()+ " "
                    + mergeControl.getRadarLeagueName() +"\n";

            resultBook = mergeControl.getBookStartTime() + " " + mergeControl.getBookDuration() + " "
                    + mergeControl.getBookHome() +" __ " + mergeControl.getBookAway() + " " +
                    + mergeControl.getBookHomeScore() + " " + mergeControl.getBookAwayScore() + " "
                    + mergeControl.getBookLeague() + "\n";

            result=result+resultRadar+resultBook + "\n";
        }

        return result;
    }


    private MimeMessagePreparator getMatchControlPreparator(List<MergeControl> mergeControlList) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("jantony9080@gmail.com");
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress("jantony9080@gmail.com"));
                mimeMessage.setText(getMessageText(mergeControlList));
                mimeMessage.setSubject("Match Control "+timestamp);
            }
        };
        return preparator;
    }

}
