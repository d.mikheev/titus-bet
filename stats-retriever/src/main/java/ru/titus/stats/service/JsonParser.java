package ru.titus.stats.service;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import ru.titus.model.sportradar.Event;
import ru.titus.model.sportradar.MatchDetails;
import ru.titus.model.sportradar.MatchInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class JsonParser {

    private static String deleteQuotes(String vsQuotes) {
        return vsQuotes.substring(1, vsQuotes.length() - 1);
    }

    private static boolean matchOnLive(JsonNode node) {
        Boolean result = false;
        JsonNode statusNode = node.path("status");
        String status = deleteQuotes(statusNode.path("name").toString());
        if (status.matches("1st half") || status.matches("2nd half"))
            result = true;
        return result;
    }

    public static int getMatchAddTime(String json) {
        int mainTime = getMatchMainTime(json);
        int injuryTime = 0;
        int currentMainTime = 0;
        int currentAddTime = 0;
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(json);
            JsonNode docRoot = root.path("doc");
            for (JsonNode docNode : docRoot) {
                JsonNode data = docNode.path("data");
                data = data.path("data");
                for (JsonNode dataNode : data) {
                    currentMainTime = dataNode.path("time").asInt();
                    if (mainTime == currentMainTime) {
                        currentAddTime = dataNode.path("injurytime").asInt();
                        if (injuryTime < currentAddTime)
                            injuryTime = currentAddTime;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return injuryTime;
    }

    public static int getMatchMainTime(String json) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(json);
            JsonNode docRoot = root.path("doc");
            for (JsonNode docNode : docRoot) {
                JsonNode data = docNode.path("data");
                data = data.path("data");
                for (JsonNode dataNode : data) {
                    int currentTime = dataNode.path("time").asInt();
                    if (currentTime > 0)
                        return currentTime;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private static String paramStringJackson(JsonNode node, String paramName, String homeOrAway) {
        node = node.path(paramName);
        node = node.path(homeOrAway);
        String result = node.toString();
        result = deleteQuotes(result);
        return result;
    }

    private static int paramIntJackson(JsonNode node, String paramName, String homeOrAway) {
        node = node.path(paramName);
        node = node.path("value");
        node = node.path(homeOrAway);
        int result = node.asInt();
        return result;
    }

    private static Event fillLiveEventJackson(JsonNode node, String realCategory, String tournament, String seasonType, String tournamentLevel) {
        Event event = new Event();
        event.setLeagueCountry(realCategory);
        event.setLeagueName(tournament);
        event.setSeasonType(seasonType);
        event.setTournamentLevel(tournamentLevel);
        event.setMatchid(node.path("_id").asInt());
        JsonNode dtNode = node.path("_dt");
        event.setTimeStart(deleteQuotes(dtNode.path("time").toString()));
        event.setDateStart(deleteQuotes(dtNode.path("date").toString()));
        event.setTzStart(deleteQuotes(dtNode.path("tz").toString()));
        event.setUts(dtNode.path("uts").asInt());
        JsonNode teamsNode = node.path("teams");
        JsonNode homeNode = teamsNode.path("home");
        event.setHome(deleteQuotes(homeNode.path("name").toString()));
        event.setHomeMediumName(deleteQuotes(homeNode.path("mediumname").toString()));
        JsonNode awayNode = teamsNode.path("away");
        event.setAway(deleteQuotes(awayNode.path("name").toString()));
        event.setAwayMediumName(deleteQuotes(awayNode.path("mediumname").toString()));
        JsonNode resultNode = node.path("result");
        event.setScoreHome(resultNode.path("home").asInt());
        event.setScoreAway(resultNode.path("away").asInt());
        JsonNode statusNode = node.path("status");
        event.setStatus(deleteQuotes(statusNode.path("name").toString()));


        return event;
    }

    public static List<Event> parseEvents(String json) {
        List<Event> liveEvents = new ArrayList<Event>();
        String realCategory = "";
        String tournament = "";
        String seasonType = "";
        String tournamentLevel = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(json);
            JsonNode docRoot = root.path("doc");
            for (JsonNode docNode : docRoot) {
                JsonNode data = docNode.path("data");
                for (JsonNode dataNode : data) {
                    JsonNode name = dataNode.path("name");
                    if (deleteQuotes(name.toString()).matches("Soccer")) {
                        JsonNode realCategories = dataNode.path("realcategories");
                        for (JsonNode realCategoryNode : realCategories) {
                            JsonNode realCategoryName = realCategoryNode.path("name");
                            realCategory = deleteQuotes(realCategoryName.toString());
                            JsonNode tournaments = realCategoryNode.path("tournaments");
                            for (JsonNode tournamentNode : tournaments) {
                                JsonNode tournamentName = tournamentNode.path("name");
                                tournament = deleteQuotes(tournamentName.toString());
                                JsonNode seasonTypeName = tournamentNode.path("seasontypename");
                                seasonType = deleteQuotes(seasonTypeName.toString());
                                JsonNode tournamentLevelName = tournamentNode.path("tournamentlevelname");
                                tournamentLevel = deleteQuotes(tournamentLevelName.toString());
                                JsonNode matches = tournamentNode.path("matches");
                                for (JsonNode matchNode : matches) {
                                    if (matchOnLive(matchNode)) {
                                        Event event = new Event();
                                        event = fillLiveEventJackson(matchNode, realCategory, tournament, seasonType, tournamentLevel);
                                        liveEvents.add(event);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return liveEvents;

    }

    public static MatchInfo parseMatchInfo(String json) {
        MatchInfo matchInfo = new MatchInfo();
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(json);
            JsonNode docRoot = root.path("doc");
            for (JsonNode node : docRoot) {
                JsonNode data = node.path("data");
                JsonNode tournament = data.path("tournament");
                matchInfo.setTournamentid(tournament.path("_id").asInt());
                matchInfo.setTournamentName(tournament.path("name").asText());
                matchInfo.setTournamentSeasonTypeName(tournament.path("seasontypename").asText());
                JsonNode stadium = data.path("stadium");
                matchInfo.setStadiumid(stadium.path("_id").asInt());
                matchInfo.setStadiumName(stadium.path("name").asText());
                matchInfo.setStadiumCity(stadium.path("city").asText());
                matchInfo.setStadiumCountry(stadium.path("country").asText());
                matchInfo.setStadiumCapacity(stadium.path("capacity").asInt());
                matchInfo.setStadiumGoogleCoords(stadium.path("googlecoords").asText());
                JsonNode match = data.path("match");
                matchInfo.setMatchid(match.path("_id").asInt());
                matchInfo.setStadiumid(match.path("stadiumid").asInt());
                matchInfo.setWeather(match.path("weather").asInt());
                matchInfo.setPitchCondition(match.path("pitchcondition").asInt());
                JsonNode teams = match.path("teams");
                JsonNode home = teams.path("home");
                matchInfo.setHomeTeamid(home.path("_id").asInt());
                matchInfo.setHome(home.path("name").asText());
                matchInfo.setHomeMediumName(home.path("mediumname").asText());
                JsonNode away = teams.path("away");
                matchInfo.setAwayTeamid(away.path("_id").asInt());
                matchInfo.setAway(away.path("name").asText());
                matchInfo.setAwayMediumName(away.path("mediumname").asText());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return matchInfo;
    }

    public static MatchDetails parseMatchDetails(String json) {
        MatchDetails matchDetails = new MatchDetails();
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode root = mapper.readTree(json);
            JsonNode docRoot = root.path("doc");
            for (JsonNode node : docRoot) {
                JsonNode data = node.path("data");
                matchDetails.setMatchid(data.path("_matchid").asLong());
                matchDetails.setHome(paramStringJackson(data, "teams", "home"));
                matchDetails.setAway(paramStringJackson(data, "teams", "away"));
                JsonNode value = data.path("values");
                matchDetails.setyCardsHome(paramIntJackson(value, "40", "home"));
                matchDetails.setyCardsAway(paramIntJackson(value, "40", "away"));
                matchDetails.setrCardsHome(paramIntJackson(value, "50", "home"));
                matchDetails.setrCardsAway(paramIntJackson(value, "50", "away"));
                matchDetails.setSubstitutionHome(paramIntJackson(value, "60", "home"));
                matchDetails.setSubstitutionAway(paramIntJackson(value, "60", "away"));
                matchDetails.setBallPosHome(paramIntJackson(value, "110", "home"));
                matchDetails.setBallPosAway(paramIntJackson(value, "110", "away"));
                matchDetails.setFreeKicksHome(paramIntJackson(value, "120", "home"));
                matchDetails.setFreeKicksAway(paramIntJackson(value, "120", "away"));
                matchDetails.setGoalKicksHome(paramIntJackson(value, "121", "home"));
                matchDetails.setGoalKicksAway(paramIntJackson(value, "121", "away"));
                matchDetails.setTrowsHome(paramIntJackson(value, "122", "home"));
                matchDetails.setTrowsAway(paramIntJackson(value, "122", "away"));
                matchDetails.setOffsHome(paramIntJackson(value, "123", "home"));
                matchDetails.setOffsAway(paramIntJackson(value, "123", "away"));
                matchDetails.setCornerHome(paramIntJackson(value, "124", "home"));
                matchDetails.setCornerAway(paramIntJackson(value, "124", "away"));
                matchDetails.setShotsOnHome(paramIntJackson(value, "125", "home"));
                matchDetails.setShotsOnAway(paramIntJackson(value, "125", "away"));
                matchDetails.setShotsOffHome(paramIntJackson(value, "126", "home"));
                matchDetails.setShotsOffAway(paramIntJackson(value, "126", "away"));
                matchDetails.setSaveHome(paramIntJackson(value, "127", "home"));
                matchDetails.setSaveAway(paramIntJackson(value, "127", "away"));
                matchDetails.setFoulsHome(paramIntJackson(value, "129", "home"));
                matchDetails.setFoulsAway(paramIntJackson(value, "129", "away"));
                matchDetails.setInjuriesHome(paramIntJackson(value, "158", "home"));
                matchDetails.setInjuriesAway(paramIntJackson(value, "158", "away"));
                matchDetails.setDattackHome(paramIntJackson(value, "1029", "home"));
                matchDetails.setDattackAway(paramIntJackson(value, "1029", "away"));
                matchDetails.setBallSafeHome(paramIntJackson(value, "1030", "home"));
                matchDetails.setBallSafeAway(paramIntJackson(value, "1030", "away"));
                matchDetails.setAttackHome(paramIntJackson(value, "1126", "home"));
                matchDetails.setAttackAway(paramIntJackson(value, "1126", "away"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return matchDetails;
    }

}
