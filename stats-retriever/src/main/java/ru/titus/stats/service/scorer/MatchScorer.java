package ru.titus.stats.service.scorer;

import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.sportradar.Event;

public interface MatchScorer {
    int score(BookMatch bookMatch, Event event);
}
