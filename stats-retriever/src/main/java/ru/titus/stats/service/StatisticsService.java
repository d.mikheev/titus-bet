package ru.titus.stats.service;

public interface StatisticsService {

    public String retrieveStats(long matchId);

    public String retrieveInfo(long matchId);

}
