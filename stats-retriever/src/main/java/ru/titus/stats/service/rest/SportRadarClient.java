package ru.titus.stats.service.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SportRadarClient {

    @GET("betradar/en/Europe:Berlin/gismo/match_detailsextended/{matchid}")
    Call<String> detailsExtended(@Path("matchid") long matchId);

    @GET("betradar/en/Europe:Berlin/gismo/match_info/{matchid}")
    Call<String> matchInfo(@Path("matchid") long matchId);

    @GET("betradar/en/Europe:Berlin/gismo/event_fullfeed/{date_offset}/{service_id}")
    Call<String> getMatchList(@Path("date_offset") long dateOffset,@Path("service_id")int serviceId);

    @GET("common/en/Europe:Berlin/gismo/stats_match_situation/{matchid}")
    Call<String> getMatchSituation(@Path("matchid") long matchId);
}
