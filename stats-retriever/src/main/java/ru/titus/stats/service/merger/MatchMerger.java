package ru.titus.stats.service.merger;

import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.sportradar.Event;

import java.util.List;
import java.util.Optional;

public interface MatchMerger {
    Optional<BookMatch> mergeMatch(BookMatch bookMatch, List<Event> liveSportRadarMatches);
}
