package ru.titus.stats.service.merger;

import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.sportradar.Event;
import ru.titus.stats.service.scorer.MatchScorerImpl;

import java.util.List;
import java.util.Optional;

public class ScoringMatchMerger implements MatchMerger {
    private MatchScorerImpl scorer = new MatchScorerImpl();

    @Override
    public Optional<BookMatch> mergeMatch(BookMatch bookMatch, List<Event> liveSportRadarMatches) {
        for (Event liveSportRadarMatch : liveSportRadarMatches) {
            int score = scorer.score(bookMatch, liveSportRadarMatch);
            //FIXME: should not use the constants from Implementation
            if (score >= MatchScorerImpl.TOTAL_SCORE) {
                bookMatch.setMatchid(liveSportRadarMatch.getMatchid());
                bookMatch.setMainTime(liveSportRadarMatch.getMainTime());
                bookMatch.setAdditionalTime(liveSportRadarMatch.getAdditionalTime());
                bookMatch.setMergeScore(score);
                return Optional.of(bookMatch);
            }
        }
        return Optional.empty();
    }
}
