package ru.titus.stats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.titus.feed.NormalSubscriber;
import ru.titus.stats.service.StatsCrawler;
import ru.titus.stats.service.email.EmailService;
import ru.titus.stats.service.rest.InfoService;

@RestController
public class MatchExtendedStatsController {

    @Autowired private InfoService infoService;
    @Autowired private StatsCrawler statsCrawler;
    @Autowired private EmailService emailService;

    @RequestMapping("/mail")
    public void test(){
        statsCrawler.emailMergeControl();
    }

    @RequestMapping("/run")
    public void start() {
        statsCrawler.retrieveAndSaveMatchData();
    }

//    @RequestMapping("/fullMatchInfo")
//    public void getFullMatchesInfo(){
//        statsCrawler.retrieveFullMatchInfo();
//    }

    @RequestMapping("/stats/{matchId}")
    public String retreiveStats(@PathVariable int matchId) {
        return infoService.retrieveStats(matchId);
    }

    @RequestMapping("/go")
    public void getLiveMatchOdds(){
        NormalSubscriber normalSubscriber=new NormalSubscriber();
        normalSubscriber.start();
    }

}
