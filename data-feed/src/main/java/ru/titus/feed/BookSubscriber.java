package ru.titus.feed;

import ru.titus.model.datafeed.BookMatch;
import ru.titus.model.datafeed.MatchOdd;
import jayeson.lib.datastructure.Record;
import jayeson.lib.datastructure.SoccerEvent;
import jayeson.lib.datastructure.SoccerEventLiveState;
import jayeson.lib.recordfetcher.DeltaCrawlerSession;
import jayeson.lib.recordfetcher.DeltaEventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class BookSubscriber {

    @Autowired
    private DeltaCrawlerSession cs;
    private static final int TOTALorHDP=1;
    private static final int ONETWO=2;

    private MatchOdd fillOddFromRecord(Record r, MatchOdd matchOdd) {
        matchOdd.setOddId(r.getOddId());
        matchOdd.setSource(r.getSource());
        matchOdd.setOddType(r.getOddType().dispText());
        matchOdd.setPivotType(r.getPivotType().toString());
        matchOdd.setPivotValue(r.getPivotValue());
        matchOdd.setRateUnderUid(r.getRateUnderUid());
        matchOdd.setRateUnder(r.getRateUnder());
        matchOdd.setRateOverUid(r.getRateOverUid());
        matchOdd.setRateOver(r.getRateOver());
        matchOdd.setRateEqualUid(r.getRateEqualUid());
        matchOdd.setRateEqual(r.getRateEqual());
        matchOdd.setTimeType(r.getTimeType().toString());
        matchOdd.setPivotBias(r.getPivotBias().toString());
        matchOdd.setLbType(r.getLBType().toString());
        return matchOdd;
    }

    public List<BookMatch> getBookMatches()
    {
        List<BookMatch> newBookMatches = new ArrayList<BookMatch>();
        cs.connect();
        cs.waitConnection();
        Collection<SoccerEvent> events = cs.getAllEvents();
        System.out.println("-------------------"+events.size()+" events------------------------------------------------------------------");
        for (SoccerEvent e : events) {
            BookMatch newBookMatch = new BookMatch();
            Collection<Record> rs = e.getRecords();

            if (rs.size() == 0) continue;
            SoccerEventLiveState state = e.getLiveState();

            if (state.getSegment().value()!=0) {

                System.out.println(String.format("Id %s \t-\t Host %s \t-\t Guest %s \t-\t League %s", e.getEventId(), e.getHost(), e.getGuest(), e.getLeague()));
                newBookMatch.setBookId(e.getEventId());
                newBookMatch.setHost(e.getHost());
                newBookMatch.setGuest(e.getGuest());
                newBookMatch.setLeague(e.getLeague());
                if (e.getLeague().contains("Corner"))
                {}
                else {
                    Collection<Record> rrs = e.getRecords();
                    for (Record r : rrs) {
                        MatchOdd matchOdd = new MatchOdd();
                        matchOdd = fillOddFromRecord(r,matchOdd);
                        newBookMatch.addMatchOdd(matchOdd);
                        System.out.println("(" + r.getOddId() + " " + r.getSource() + " " + r.getOddType() + " " + r.getPivotType() + " " + r.getPivotValue() + " " + r.getPivotString() + " " + " " + r.getRateOverUid() + " " + r.getRateOver() + " " + r.getRateUnderUid() + " " + r.getRateUnder() + " " + r.getRateEqualUid() + " " + r.getRateEqual() + ")");
                    }

                    System.out.println(String.format("LiveState Information---- Starttime: %d, Source: %s, Duration: %d, Score %d-%d -", state.getStartTime(), state.getSource(), state.getDuration(), state.getHostPoint(), state.getGuestPoint()));
                    newBookMatch.setStartTime(state.getStartTime());
                    newBookMatch.setDuration(state.getDuration());
                    newBookMatch.setScoreHome(state.getHostPoint());
                    newBookMatch.setScoreAway(state.getGuestPoint());
                    newBookMatches.add(newBookMatch);

                    System.out.println(state.getSegment());
                    System.out.println(state.getSegment().value());
                    System.out.println(state.getSegment().toShortString());

                    Map<String, String> oids = e.getAllOriginalEventIds();
                    System.out.println("Original event ids");
                    for (Map.Entry<String, String> et : oids.entrySet()) {
                        System.out.print(et.getKey() + " - " + et.getValue() + " ");
                    }
                    System.out.println();
                }
            }
        }

        return newBookMatches;
    }

}
