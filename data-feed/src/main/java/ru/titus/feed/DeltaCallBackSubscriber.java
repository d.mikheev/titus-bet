package ru.titus.feed;
/**
 *
 * @author Praveen
 */

import jayeson.lib.recordfetcher.DeltaCrawlerSession;

import java.util.Properties;

public class DeltaCallBackSubscriber {
	
	public void start() {

		Properties systemProps = System.getProperties();
	
		// setup key stores for secure connections
		systemProps.put("javax.net.ssl.trustStore", "conf/client.ts");
		systemProps.put("javax.net.ssl.keyStore", "conf/client.ks");
		systemProps.put("javax.net.ssl.trustStorePassword", "password");
		systemProps.put("javax.net.ssl.keyStorePassword", "password");
		//setup the configuration file
		systemProps.put("deltaCrawlerSessionConfigurationFile", "conf/deltaCrawlerSession.json");
		
		DeltaCrawlerSession cs = new DeltaCrawlerSession();
		cs.setSubscriberRestartInterval(0);
		
		cs.connect();
		cs.waitConnection();
		
		DeltaFeedTracker dft = new DeltaFeedTracker();
		cs.addDeltaEventHandler(dft);

//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		cs.disconnect();
//		System.out.println("Exit");
//		System.exit(0);
//		while (true) {
//	
//			try {
//				Thread.sleep(5000);
//			} catch (Exception ex) {
//
//			}
//		}
		
	}
}
