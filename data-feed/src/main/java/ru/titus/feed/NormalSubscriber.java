package ru.titus.feed;

/**
 *
 * @author Praveen
 */

import jayeson.lib.datastructure.Record;
import jayeson.lib.datastructure.SoccerEvent;
import jayeson.lib.datastructure.SoccerEventLiveState;
import jayeson.lib.recordfetcher.DeltaCrawlerSession;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class NormalSubscriber {

	public void start() {

		Properties systemProps = System.getProperties();
		// setup key stores for secure connections
		systemProps.put("javax.net.ssl.trustStore", "conf/client.ts");
		systemProps.put("javax.net.ssl.keyStore", "conf/client.ks");
		systemProps.put("javax.net.ssl.trustStorePassword", "password");
		systemProps.put("javax.net.ssl.keyStorePassword", "password");
		//setup the configuration file
		systemProps.put("deltaCrawlerSessionConfigurationFile", "conf/deltaCrawlerSession.json");


		DeltaCrawlerSession cs = new DeltaCrawlerSession();

		// this will enable auto reconnection for your record fetcher in case of connection failure
		cs.connect();

//		cs.connect("wM9hDeqQNE_grp80","wM9hDeqQNE","ssl://datafeed-01.olesportsresearch.com:61617?connectionTimeout=60000");
		cs.waitConnection();


		while (true) {
			// this call will block if the connection to the feed fails.
			// If auto reconnection is enabled, it will automatically reconnect to the feed for you
			cs.waitConnection();
			Collection<SoccerEvent> events = cs.getAllEvents();
			System.out.println("-------------------"+events.size()+" events------------------------------------------------------------------");
			for (SoccerEvent e : events) {

				Collection<Record> rs = e.getRecords();

				if (rs.size() == 0) continue;
				SoccerEventLiveState state = e.getLiveState();

				if (state.getSegment().value()!=0) {

					System.out.println(String.format("Id %s \t-\t Host %s \t-\t Guest %s \t-\t League %s", e.getEventId(), e.getHost(), e.getGuest(), e.getLeague()));


				Collection<Record> rrs = e.getRecords();
				for (Record r : rrs) {
				    if (r.getPivotType().toString().contains("TOTAL"))
					    System.out.println("("+r.getOddId() + " " +r.getSource()+" "+r.getOddType()+" "+r.getPivotType()+" "+r.getPivotValue()+" "+r.getPivotString()+" "+
							" " + r.getRateOverUid() + " " +  r.getRateOver()+" "+
							r.getRateUnderUid() + " " + r.getRateUnder()+" "+
							r.getRateEqualUid() + " " + r.getRateEqual()+")");
				}


					System.out.println(String.format("LiveState Information---- Starttime: %d, Source: %s, Duration: %d, Score %d-%d -", state.getStartTime(), state.getSource(), state.getDuration(), state.getHostPoint(), state.getGuestPoint()));
					System.out.println(state.getSegment());
					System.out.println(state.getSegment().value());
					System.out.println(state.getSegment().toShortString());

					Map<String, String> oids = e.getAllOriginalEventIds();
					System.out.println("Original event ids");
					for (Entry<String, String> et : oids.entrySet()) {
						System.out.print(et.getKey() + " - " + et.getValue() + " ");
					}
					System.out.println();


					System.out.println("\n");
				}
			}


			try {


			} catch (Exception ex) {
				System.out.println("Exception");
				ex.printStackTrace();
			}

			try {
				Thread.sleep(10000);
			} catch (Exception ex) {

			}
		}


	}
}
